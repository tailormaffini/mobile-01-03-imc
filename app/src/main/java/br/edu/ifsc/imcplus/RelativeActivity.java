package br.edu.ifsc.imcplus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RelativeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative);
    }

    public void CalculaIMC(View view){
        TextView resultado = (TextView) findViewById(R.id.TextResultado);
        EditText peso = (EditText) findViewById(R.id.EditPeso);
        EditText altura = (EditText) findViewById(R.id.EditAltura);

        Double result = Double.valueOf(peso.getText().toString()) / (Double.valueOf(altura.getText().toString())*2);
        resultado.setText(String.format("%.2f",result));

    }
}
